using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Utils;
using Mapbox.Unity.Utilities;

public class Tag : MonoBehaviour{
    
    private int tagId;
    private Vector2d position;
    [SerializeField] private Text textField;
    private TagManager tagManager;
    private Text infoCanvasText;


    void Awake(){
        tagManager = GameObject.FindGameObjectWithTag("TagManager").GetComponent<TagManager>() ;
        position = this.transform.GetGeoPosition(tagManager.GetReferencePosition());
        this.GetComponentInChildren<Canvas>().worldCamera = Camera.main;
        tagId = tagManager.nextId;
        tagManager.nextId++;
    }

    public int GetId(){
        return this.tagId;
    }
    public Text GetText(){
        return this.textField;
    }
    public Vector2d GetPosition(){
        return this.position;
    }

    public void SetText(string text){
        textField.text = text;
    }

    public void Click(){
        Debug.Log("Tag no. "+tagId+" was clicked");
        tagManager.infoCanvas.SetActive(true);
        tagManager.infoCanvasText.text = this.textField.text;
    }

}
