using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 lastTargetPos;

    void Awake(){
        if(camera == null){
            camera = Camera.main;
        }
        camera.transform.SetPositionAndRotation( new Vector3( target.position.x, 
                                                camera.transform.position.y, 
                                                target.position.z - 60 ), 
                                                Quaternion.Euler( new Vector3 ( 60, 0, 0 ) ) 
                                                );

        lastTargetPos = target.position;
    }

    void Update()
    {
        if(Input.touchCount < 1){
            camera.transform.position += target.position - lastTargetPos;
        }else{
            Touch touch = Input.GetTouch(0);

            float spinSpeed = 5 * Time.deltaTime;


            if( touch.position.y > ( Screen.height / 2 ) ){
                camera.transform.RotateAround( target.position, Vector3.up, -touch.deltaPosition.x * spinSpeed );
            }else{
                camera.transform.RotateAround( target.position, Vector3.up, touch.deltaPosition.x * spinSpeed );
            } 
        }

        lastTargetPos = target.position;
    }
}
