using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateNewTag : MonoBehaviour
{

    [SerializeField] private GameObject panel;
    [SerializeField] private InputField inputField;
    private string panelMessage;

    private TagManager tagMan;

    void Awake(){

        tagMan = GameObject.FindGameObjectWithTag("TagManager").GetComponent<TagManager>();

    }

    public void SetTagPanel(){ 
        panel.SetActive(!panel.activeSelf);
    }

    public void SubmitTag(){

        tagMan.SubmitManualTag(panelMessage);
        inputField.Select();
        inputField.text = "";

        Debug.Log("New tag submitted!");
        panel.SetActive(false);
    }

    public void DiscardTag(){
        inputField.Select();
        inputField.text = "";
        panel.SetActive(false);
    }

    public void SetPanelMessage(){
        inputField.Select();
        panelMessage = inputField.text;
        Debug.Log("panelMessage: "+ panelMessage);
    }
}
