using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.Unity.Utilities;

public class TagManager : MonoBehaviour
{
    [SerializeField] private AbstractMap map;
    [SerializeField] private GameObject tagPrefab;
    private List<GameObject> tags = new List<GameObject>();
    public GameObject infoCanvas;
    public Text infoCanvasText; 

    public int nextId;

    [SerializeField] private Transform player;

    public double spawnRadius;
    private Vector2d refPosition;

    private List<string> mockStrings = new List<string>();

    void Start(){

        mockStrings.Add("Maria is the love of my life !");
        mockStrings.Add("Everybody gotta try out this hotdog stand - it AMAZING!");
        mockStrings.Add("If you appreciate a bit of water and a nice talk - try out the canaltours! Geat VALUE!");
        mockStrings.Add("Looking for the greatest cocktail in town? LOOK NO MORE!");
        mockStrings.Add("R + B 4evar <33");
        mockStrings.Add("The exhibition caught me off guard and left me breathless! Beautiful!");
        mockStrings.Add("Denmark is just great. Nice people. Great food.");
        mockStrings.Add("Dont ever rely on DSB.");
        mockStrings.Add("Denmark survival guide - avoid public transportation!");
        mockStrings.Add("I dont ever wanna leave! EVER!");


        refPosition = map.CenterMercator;

        nextId = 0;

        SpawnMockTags();

        for(int i = 0; i < mockStrings.Count; i++){

            Tag tag = tags[i].GetComponent<Tag>();
            tag.SetText(mockStrings[i]);

        }

    }

    public Vector2d GetReferencePosition(){
        return refPosition;
    }

    private void SpawnMockTags(){

            for(int i = 0;i < mockStrings.Count; i++){

                double randLat = Random.Range((float)-spawnRadius, (float)spawnRadius);
                double randLon = Random.Range((float)-spawnRadius, (float)spawnRadius);

                Vector2d processedPosition = player.GetGeoPosition(refPosition);

                processedPosition.x += randLat;
                processedPosition.y += randLon;

                Vector2d virtualPosition = Conversions.GeoToWorldPosition(processedPosition, refPosition);

                Vector3 unityWorldPos = new Vector3((float)virtualPosition.x, 0.2f, (float)virtualPosition.y);

                Quaternion newRotation = Quaternion.Euler(0, Random.Range(0.0f, 360.0f), 0);

                GameObject newTag = Instantiate(tagPrefab, unityWorldPos, newRotation);

                newTag.SetActive(true);

                Image butt = newTag.GetComponentInChildren<Image>();

                Color col = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1);
                
                butt.color = col;

                tags.Add(newTag);

            }
    }

    public void SubmitManualTag(string tagMessage){
        //get gps coordinate of player
        Vector2d tagPos = player.GetGeoPosition(refPosition); 
        //convert that into a virtual position relative to the reference point - also convert into unity units
        Vector2d virtualPosition = Conversions.GeoToWorldPosition(tagPos, refPosition); 
        //convert it into Vector3 which unity can read
        Vector3 unityWorldPos = new Vector3((float)virtualPosition.x, 0.2f, (float)virtualPosition.y);
        //create the rotation of the tag around the y axis relative to player.
        Quaternion newRotation = Quaternion.Euler(0, player.rotation.y, 0);
        //spawn it into the world at the calculated place with the calculated rotation
        GameObject newTag = Instantiate(tagPrefab, unityWorldPos, newRotation);
        //make it visible
        newTag.SetActive(true);
        //create a random color for the tag
        Image butt = newTag.GetComponentInChildren<Image>();
        Color col = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1);        
        butt.color = col;
        //set the tag message
        Tag tag = newTag.GetComponent<Tag>();
        tag.SetText(tagMessage);
        //add it to the list of all tags
        tags.Add(newTag);
    }

}
