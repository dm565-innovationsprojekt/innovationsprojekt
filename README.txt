This is the Geotagging Project by Stallone Nobert, Mads Garff and Nicolai Egede

To access our work go to /Geotagging/Geotagging/Assets/Geotagging

To watch our youtube video preview of the app prototype go to: https://www.youtube.com/watch?v=mCBc5hPt7_0

If you need a public key to access the unity project, there is one made available in this folder.

If you have an Android device and want to try it out, you can use the Geotagging.apk to install it on your device.

best Regards

Stallone, Mads & Nicolai